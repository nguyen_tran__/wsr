# -*- coding: utf-8 -*-
"""
Created on Wed Apr 19 17:23:52 2017

@author: Nguyen Tran
"""
from Kernel.SystemUtilities.initialization import WSR
import os, sys

if __name__ == "__main__":
#    wsr = WSR(sys.argv[1:])  
    wsr = WSR(["-c", ".\configs\init.json", "-m", ".\modules", "-s"])  