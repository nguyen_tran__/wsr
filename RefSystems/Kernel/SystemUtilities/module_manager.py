# -*- coding: utf-8 -*-
"""
Created on Wed Apr 19 14:48:20 2017

@author: Nguyen Tran
This script provides functions to import and load modules from a given initialization script
"""
import importlib

detectedResourcesObj = None
runningModules = []

def init(detectedRes):
    global detectedResourcesObj
    detectedResourcesObj = detectedRes


"""
Return objects representing modules specified in the moduleInfos, an additional set of
system parameter object can be passed to all modules loaded with this function call

The type checking of the module is only performed at the top level.
The validity of sub modules and system parameters is maintained by the module itself.
"""
def loadModules(moduleInfos, systemParams = None, moduleTypes = None):
    modules = []
    for moduleInfo in moduleInfos:
        module = _loadModule(moduleInfo, systemParams)
        # Check the type of module to see whether user provided the appropriate type of module
        if not(moduleTypes):
            pass
        else:
            if not(module.type in moduleTypes):
                raise TypeError("Invalid module type: module %s is not in %s." % (moduleInfo, moduleTypes))
        modules.append(module)
        print("Loaded module %s" % moduleInfo["name"])
    return modules


"""
Stop all running modules
"""
def stopAllModules():
    for module in runningModules:
        module.stop()
    print("All running modules are stopped")
    
"""
Create an individual module.
"""
def _loadModule(moduleInfo, systemParams = None):
    # Import base module
    moduleName = moduleInfo.get("name")
    modulePackage = moduleInfo.get("package")
    moduleParams = moduleInfo.get("params")
    moduleStart = moduleInfo.get("start")
    moduleConstructor = _getModuleConstructor(moduleName, modulePackage)
    
    # Import sub modules
    subModuleInfos = moduleInfo.get("subModules", [])
    subModules = []
    if(len(subModuleInfos) > 0):
        subModules = loadModules(subModuleInfos)
    
    # Construct the module object
    module = moduleConstructor(systemParams, moduleParams, subModules);
    
    # Check whether the given module is of the appropriate type
    
    # If module is configured to auto start, call its start function
    if moduleStart:
        module.start()
        runningModules.append(module)
        
    return module
    
def _getModuleConstructor(moduleName, modulePackage):
#    print("%s.%s" % (modulePackage, moduleName))
    module = importlib.import_module("%s.%s" % (modulePackage, moduleName))
    return getattr(module, moduleName)
    
