# -*- coding: utf-8 -*-
"""
Created on Sun Apr 23 12:13:55 2017

@author: Nguyen Tran
This class represents a user request sent from user interface to the query scorer
"""
class Request(object):
    def __init__(self, clientInfo, query):
        if not(getattr(query, "type", "") == "QUERY"):
            raise RuntimeError("The query object given to this request is not a Query object")
        else:
            self.clientInfo = clientInfo
            self.query = query
            self.type = "REQUEST"
