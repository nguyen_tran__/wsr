# -*- coding: utf-8 -*-
"""
Created on Wed Apr 05 11:42:31 2017

@author: Nguyen Tran
"""
class Result (object):
    def __init__(self, error = False, errorMes = ""):
        self.type = "RESULT"
        self.resList = []
        self.isError = error
        if(error):
            self.error = errorMes
            
    def __str__(self):
        if self.isError:
            return "Error: %s" %  self.error
        else:
            return "Results: %s" % self.resList
        
    """
    Result object doesn't concern with the type of supplied item and score.
    Validating the type of item and score is the responsibility of modules using them
    """
    def addResult(self, item, score):
        res = {"item" : item, "score" : score}
        self.resList.append(res)
        
    """
    Assume that each result is a tuple
    """
    def addResults(self, results):
        if type(results) is not list:
            raise TypeError("Results to add to the result object must be a list")
        for result in results:
            try:
                self.addResult(result[0], result[1])
            except:
                raise RuntimeError("The result to add into the list must be a tuple of (item, score)")
    
    """
    Removing a result from the result list object
    """        
    def removeResult(self, index):
        if index >= 0 and index < len(self.resList):
            del self.resList[index]
        else:
            raise IndexError("The index of the item to remove must be between 0 and %s" % (len(self.resList) - 1))
        
    def removeResults(self, indices):
        if type(indices) is not list:
            raise TypeError("Indices to remove must be a list")
        # Remove duplication
        indices  = set(indices)
        indices = list(indices)
        # Sort the indices to delete
        indices.sort(reverse = True)
        for index in indices:
            self.removeResult(index)

if __name__ == "__main__":
    r = Result()
    r.addResult("Sensor 1", 9)
    r.addResult("Sensor 2", 12)
    r.addResults([("Sensor 3", 1), ("Sensor 4", 3), ("Sensor 5", 11)])
    r.removeResults([1,1,4,3])
    print(r.resList)
