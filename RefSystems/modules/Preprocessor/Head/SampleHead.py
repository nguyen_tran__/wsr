# -*- coding: utf-8 -*-
"""
Created on Thu Mar 30 16:57:22 2017

@author: Nguyen Tran
"""
from Abstracts.Abstracts import AbsPreProcHead

class SampleHead(AbsPreProcHead):
    def __init__(self, *args):
        super(SampleHead, self).__init__(*args)
        
    def _processDatum(self, datum, result):
#        print("Finished the chain")
        self.storageManager.create(datum)
