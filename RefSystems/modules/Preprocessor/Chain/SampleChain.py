# -*- coding: utf-8 -*-
"""
Created on Thu Mar 30 16:40:38 2017

@author: Nguyen Tran
"""
from Abstracts.Abstracts import AbsPreProcChain

class SampleChain(AbsPreProcChain):
    def __init__(self, *args):
        super(SampleChain, self).__init__(*args)
        
    def _processDatum(self, datum, result):
#        print("received a datum inside chain:")
#        print(datum)
        pass
