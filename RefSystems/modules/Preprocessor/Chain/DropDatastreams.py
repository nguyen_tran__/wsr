#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  3 15:52:02 2017

@author: Nguyen Tran
"""
from Abstracts.Abstracts import AbsPreProcChain
class DropDatastreams (AbsPreProcChain):
    def __init__(self, *args):
        super(DropDatastreams, self).__init__(*args)
        
    def _processDatum(self, datum, result):
        try:
            datum["content"].pop("Datastreams")
        except:
            print("Datum does not contain Datastreams attribute : %s" % datum)
            pass
