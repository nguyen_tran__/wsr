#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  3 15:03:56 2017

@author: Nguyen Tran
"""
from Abstracts.Abstracts import AbsPreProcChain
class EscapeDotChain(AbsPreProcChain):
    def __init__(self, *args):
        super(EscapeDotChain, self).__init__(*args)
    
    def _processDatum(self, datum, result):
        self._traverseKeys(datum)
    
    def _traverseKeys(self, dictionary, depth = 0, depthLim = 50):
        if depth > depthLim:
            raise RuntimeError("Depth of given datum exceeds the depth limit. The datum might be a loop. Abort processing.")
        keys = dictionary.keys()
        for key in keys:
            value = dictionary[key]
            if type(value) == list:
                for item in value:
                    self._traverseKeys(item, depth + 1)
            elif type(value) == dict:
                self._traverseKeys(value, depth + 1)
            self._changeDictKey(dictionary, key)
            
    def _changeDictKey(self, dictionary, key):
        newkey = key.replace(".", "[dot]")
        dictionary[newkey] = dictionary.pop(key)
    
def traverseKeys(dictionary, depth = 0, depthLim = 50):
    if depth > depthLim:
        raise RuntimeError("Depth of given datum exceeds the depth limit. The datum might be a loop. Abort processing.")
    keys = dictionary.keys()
    for key in keys:
        value = dictionary[key]
        if type(value) == list:
            for item in value:
                traverseKeys(item, depth + 1)
        elif type(value) == dict:
            traverseKeys(value, depth + 1)
        changeDictKey(dictionary, key)
    
def changeDictKey(dictionary, key):
    newkey = "NEW-%s" % key
    dictionary[newkey] = dictionary.pop(key)
