# -*- coding: utf-8 -*-
"""
Created on Sun Apr 16 11:04:10 2017

@author: Nguyen Tran
"""
from SharedUtilities.AbstractHTTPServer import AbsHTTPInterface
from BaseHTTPServer import BaseHTTPRequestHandler,HTTPServer
from Abstracts.Query import Query
import threading

class HTTPInterface(AbsHTTPInterface):
    class myHandler(BaseHTTPRequestHandler):
        def do_GET(self):
            import json
            self.send_response(200)
            self.send_header('Content-type','text/html')
            self.end_headers()
            # Send the html message
            res = {"hello" : "hello world"}
            self.wfile.write(json.dumps(res))
            return
        
        def do_POST(self):
            import json
            self.data_string = self.rfile.read(int(self.headers['Content-Length']))
    
            # Load query
            try:
                reqJSON = json.loads(self.data_string)
#                print("Received: %s" % reqJSON)
            except ValueError:
                self.send_response(500)
                self.send_header('Content-type','application/json')
                self.end_headers()
                res = {"Err" : "Given data is not a valid JSON"}
                self.wfile.write(json.dumps(res))
                print("The given request is not a valid JSON")
                return
            print("Received request from %s:%s" % self.client_address)
            
            # Get a scorer and pass query to the scorer
            queryQueue, resultQueue = self.interfaceObject._getScorerConn()
            self.interfaceObject._putRequest((self.client_address[0], self.client_address[1]), reqJSON, queryQueue)
    
            # Receive the response from scorer head and send response message
            for i in range(self.tries):
                response = self.interfaceObject._getResponse((self.client_address[0], self.client_address[1]), resultQueue)
                if response:
                    break
                if not(response) and i == self.tries - 1:
                    print("====Timeout while fetching response for %s:%s" % self.client_address)
                    self.send_error(500, message = "Timeout: Cannot get response.")
                    self.interfaceObject._returnScorerConn(queryQueue, resultQueue)
                    return
            results = response.result.resList
            self.send_response(200)
            self.send_header('Content-type','application/json')
            self.end_headers()
            self.interfaceObject._returnScorerConn(queryQueue, resultQueue)
#            print("Serving from: %s" % threading.currentThread())
            try:
                self.wfile.write(json.dumps(results))
#                print("wrote JSON!")
            except:
                self.wfile.write(results)
            return
    
    def __init__(self, *args):
        super(HTTPInterface, self).__init__(*args)
        self.handler = self.myHandler
        self.myHandler.interfaceObject = self
        self.myHandler.tries = 1000000
        
    def _compClientInfo(self, pendingClientInfo, clientInfo):
        if pendingClientInfo == clientInfo:
            return True
        else:
            return False
        
    def _createQueryObject(self, queryInfo):
        return Query(*queryInfo)
