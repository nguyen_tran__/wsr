# -*- coding: utf-8 -*-
"""
Created on Sun Apr 02 14:25:42 2017

@author: Nguyen Tran
"""

from Abstracts.Abstracts import AbsCollectorModule
import time

class STCollector (AbsCollectorModule):
    def __init__(self, *args):
        super(STCollector, self).__init__(*args)
        self.addAttribute("refreshRate", self.moduleParams, "refreshRate")
        self.processedSensors = []
        self.processedStreams = []
        
    def _collect(self):
        if self.resType == "SENSOR":
            self._collectSensor()
        elif self.resType == "DATASTREAM":
            self._collectDatastream()
        elif self.resType == "OBSERVATION":
            self._collectObservation()
        else:
            raise ValueError("The required type of content to collect is unknown")
            
    def _collectSensor(self):
        currSensors = self.detectedRes.getSensors()
        newSensorCount = len(currSensors) - len(self.processedSensors)
        if newSensorCount > 0:
            for sensor in currSensors[len(self.processedSensors):]:
                # Get JSON representation of the sensor 
                sensorJSON = self.connClient.getResponseJSON(sensor)
                
                # GET JSON representation of sensor's datastreams
                datastreamsJSON = self.connClient.getResponseJSON("%s/Datastreams" % sensor)
                sensorJSON["Datastreams"] = datastreamsJSON.get("value")
                
                collectionResult = {}
                collectionResult["sensor"] = sensorJSON.get("@iot.id")
                collectionResult["content"] = {
                        "sensorID" : sensorJSON.get("@iot.id"),
                        "sensor" : sensorJSON
                }
                self._addToQueue(collectionResult)
                self.processedSensors.append(sensor)
        time.sleep(self.refreshRate)
        
    def _collectDatastream(self):
        currDatastreams = self.detectedRes.getDatastreams()
        newStreamCount = len(currDatastreams) - len(self.processedStreams)
        if newStreamCount > 0:
            for datastream in currDatastreams[len(self.processedStreams):]:
                datastreamJSON = self.connClient.getResponseJSON(datastream)
                collectionResult = {}
                collectionResult["stream"] = datastreamJSON.get("@iot.id")
                collectionResult["content"] = {
                    "streamID" : datastreamJSON.get("@iot.id"),
                    "stream" : datastreamJSON
                }
                self._addToQueue(collectionResult)
                self.processedStreams.append(datastream)
    #            print("Received %s" % collectionResult)
        time.sleep(self.refreshRate)
#        print("==================\n")
        
    def _collectObservation(self):
        currDatastreams = self.detectedRes.getDatastreams()
        for datastream in currDatastreams:
            datastreamJSON = self.connClient.getResponseJSON(datastream)
            observationJSON = self.connClient.getResponseJSON("%s/Observations" % datastream)
#            print("Observation result from datastream %s : %s" % (datastreamJSON.get("@iot.id"), result))
#            print("collecting datastream %s" % datastream)
            collectionResult = {}
            collectionResult["stream"] = datastreamJSON.get("@iot.id")
            collectionResult["content"] = {
                "streamID" : datastreamJSON.get("@iot.id"),
                "observationID" : observationJSON.get("@iot.id"),
                "observation" : observationJSON
            }
            self._addToQueue(collectionResult)
#            print("Received %s" % collectionResult)
        time.sleep(self.refreshRate)
#        print("==================\n")
