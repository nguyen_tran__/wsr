# -*- coding: utf-8 -*-
"""
Created on Sun Apr 02 15:41:35 2017

@author: Nguyen Tran
"""
from Abstracts.Abstracts import AbsDetectorModule
import time

class STSensDetector (AbsDetectorModule):
    def __init__(self, *args):
        super(STSensDetector, self).__init__(*args)
        self.processedSources = []
        self.addAttribute("refreshRate", self.moduleParams, "refreshRate")

    def _detectRes(self):
#        print("From thread %s: _detectSensor was called" % self.t.name)
        currSources = self.detectedRes.getSources()
        newSourcesCount = len(currSources) - len(self.processedSources)
        if newSourcesCount > 0:
            newSources = [x for x in currSources[len(self.processedSources):len(currSources)]]
#            print("From thread %s: new sources detected" % self.t.name)
#            print(newSources)
            for source in newSources:
                # Override this part to add logic to actually crawl for sensors on this host
                for sensorJSON in self._getSensorsFromSource(source):
                    sensorURL = self._getSensorURL(source, sensorJSON)
                    self._addRes(sensorURL)
                self.processedSources.append(source)
#        print(self.detectedRes.sensorURLs)
        time.sleep(self.refreshRate)
        # No stop provided so this thread runs endlessly
        
    def _getSensorsFromSource(self, sourceURL):
        return self.connClient.getResponseJSON(sourceURL, "Sensors").get("value")
        
    def _getSensorURL(self, sourceURL, sensorJSON):
        sensorID = sensorJSON.get("@iot.id")
        if sourceURL[-1] == "/":
            return "%sSensors(%s)" % (sourceURL, sensorID)
        else:
            return "%s/Sensors(%s)" % (sourceURL, sensorID)
        
        