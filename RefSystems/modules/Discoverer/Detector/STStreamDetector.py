# -*- coding: utf-8 -*-
"""
Created on Sun Apr 02 15:28:59 2017

@author: Nguyen Tran
"""
from Abstracts.Abstracts import AbsDetectorModule
import time

class STStreamDetector (AbsDetectorModule):
    def __init__(self, *args):
        super(STStreamDetector, self).__init__(*args)
        self.processedSources = []
        self.addAttribute("refreshRate", self.moduleParams, "refreshRate")
        
    def _detectRes(self):
#        print("From thread %s: _detectDatastream was called" % self.t.name)
        currSources = self.detectedRes.getSources()
        newSourceCount = len(currSources) - len(self.processedSources)
        if newSourceCount > 0:
            newSources = [x for x in currSources[len(self.processedSources):len(currSources)]]
#            print("From thread %s: new sources detected" % self.t.name)
#            print(newSources)
            for source in newSources:
                # Override this part to add logic to actually crawl for datastreams on this host
                for datastreamJSON in self._getDatastreamsFromSource(source):
                    datastreamURL = self._getDatastreamURL(source, datastreamJSON)
                    self._addRes(datastreamURL)
                self.processedSources.append(source)
        time.sleep(self.refreshRate)

    def _getDatastreamsFromSource(self, sourceURL):
        return self.connClient.getResponseJSON(sourceURL, "Datastreams").get("value")
        
    def _getDatastreamURL(self, sourceURL, datastreamJSON):
        datastreamID = datastreamJSON.get("@iot.id")
        if sourceURL[-1] == "/":
            return "%sDatastreams(%s)" % (sourceURL, datastreamID)
        else:
            return "%s/Datastreams(%s)" % (sourceURL, datastreamID)
