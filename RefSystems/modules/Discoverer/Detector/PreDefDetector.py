# -*- coding: utf-8 -*-
"""
Created on Sun Apr 02 14:34:20 2017

@author: gentr
"""

from Abstracts.Abstracts import AbsDetectorModule
import time

class PreDefDetector (AbsDetectorModule):
    def __init__(self, *args):
        super(PreDefDetector, self).__init__(*args)
        self.addAttribute("refreshRate", self.moduleParams, "refreshRate")
        self.addAttribute("sources", self.moduleParams, "sourceList")
        
    def _detectRes(self):
        for source in self.sources:
            self._addRes(source)    
#            print("Added source %s" % source)
            time.sleep(self.refreshRate)
        self.stop()
        print("Finished adding predefined sources to the system.\n Predefined sources:")
        print(self.detectedRes.getSources())
        