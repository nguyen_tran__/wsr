# -*- coding: utf-8 -*-
"""
Created on Tue Apr 04 13:21:21 2017

@author: Nguyen Tran
"""
from Abstracts.Abstracts import AbsOneTimeIndexer as Indexer
#from Abstract.AbsPeriodIndexer import AbsPeriodIndexer as Indexer
#from Abstract.AbsReactiveIndexer import AbsReactiveIndexer as Indexer
class MongoDBIndexer (Indexer):
    def __init__(self, *args):
        super(MongoDBIndexer, self).__init__(*args)
       
    def _unpack(self, dictionary):
        return [(k, dictionary[k]) for k in dictionary]
        
    def _buildIndex(self):
        # Extract parameters for building indexes from module params
        try:
            dbName = self.moduleParams["db"]
            colName = self.moduleParams["col"]
            indexes = []
            for item in self.moduleParams["indexes"]:
                index = {}
                keys = self._unpack(item["keys"])
                index["keys"] = keys

                options = {}
                for opt in item["options"]:
#                    if type(item["options"][opt]) == dict:
#                        options[opt] = self._unpack(item["options"][opt])
#                    else:
                        options[opt] = item["options"][opt]
                index["options"] = options
                indexes.append(index)
        except:
            raise RuntimeError("The parameter given to MongoDBIndexer is invalid")
        
        # Get database connection from storage manager
        try:
            db, col = self.storageManager.getDBCol(dbName, colName)
        except:
            raise RuntimeError("Cannot get the specified database and collection from MongoDB")
            
        # Add each specified index into the database
        for index in indexes:
            try:              
                col.create_index(index["keys"], **index["options"])
            except:
                print("WARNING: Cannot add index %s" % (index))
            
#        print([i for i in col.list_indexes()])
        print("Finish building index for collection [%s] of db [%s]" % (dbName, colName))

        
    def _reIndex(self):
        print("reindex called")
        
    
