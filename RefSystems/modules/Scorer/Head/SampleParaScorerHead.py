# -*- coding: utf-8 -*-
"""
Created on Wed Apr 05 18:20:08 2017

@author: Nguyen Tran
"""
from Abstracts.Abstracts import AbsParaScorerHead
class SampleParaScorerHead (AbsParaScorerHead):
    def __init__(self, *args):
        super(SampleParaScorerHead, self).__init__(*args)
        
    def _aggregateResults(self, datum, results):
        try:
            return results[0]
        except:
            return None
