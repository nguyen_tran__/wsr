# -*- coding: utf-8 -*-
"""
Created on Wed Apr 05 15:46:38 2017

@author: Nguyen Tran
"""
from Abstracts.Abstracts import AbsSeqScorerHead
class SampleSeqScorerHead(AbsSeqScorerHead):
    def __init__(self, *args):
        super(SampleSeqScorerHead, self).__init__(*args)
        
    def _processResults(self, query, result):
        print("process results called in the sample seq scorer")
