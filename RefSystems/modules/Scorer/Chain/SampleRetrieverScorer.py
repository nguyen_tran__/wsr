# -*- coding: utf-8 -*-
"""
Created on Wed Apr 05 15:47:16 2017

@author: Nguyen Tran
"""
from Abstracts.Abstracts import AbsRetrieverScorer
class SampleRetrieverScorer (AbsRetrieverScorer):
    def __init__(self, *args):
        super(SampleRetrieverScorer, self).__init__(*args)
        
    def _retrieve(self, query, result):
        print("retrieve method called in the sample retriever scorer")
        print(query)
        result.addResult("Doc 1", 10)