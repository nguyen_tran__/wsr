# -*- coding: utf-8 -*-
"""
Created on Sun Apr 16 16:56:06 2017

@author: Nguyen Tran
This scorer runs queries on description of data streams and return the list of streams
that satisfy the needed conditions
"""
from Abstracts.Abstracts import AbsRetrieverScorer
class MetadataRetrieverScorer (AbsRetrieverScorer):
    def __init__(self, *args):
        super(MetadataRetrieverScorer, self).__init__(*args)
        
    def _retrieve(self, query, result):
        # Based on the convention of our system that the first part of the query
        # is for sensor /stream metadata 
        metadataQuery = query.SensorMetadata
        try:
            retrievedItems = self.storageManager.read(metadataQuery)
#            print("Retrieved %s item" % len(retrievedItems))
            for item in retrievedItems:
                result.addResult(item, 1)
        except:
            pass
