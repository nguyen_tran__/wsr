# -*- coding: utf-8 -*-
"""
Created on Wed Apr 05 16:59:39 2017

@author: gentr
"""
from Abstracts.Abstracts import AbsFilterScorer
class SampleFilterScorer(AbsFilterScorer):
    def __init__(self, *args):
        super(SampleFilterScorer, self).__init__(*args)

    def _filter(self, query, result):
        print("Filter method of sample filter scorer is called")