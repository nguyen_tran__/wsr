# -*- coding: utf-8 -*-
"""
Created on Mon Apr  3 11:00:35 2017

@author: nguyentran
"""

import pymongo
from pymongo import MongoClient
from datetime import datetime
import json

def testMongoDB():
    client = MongoClient()
    db = client["wsr"]
    rCol = db["streams"]
#    cursor = rCol.find({"$and" : [
#        {"cuisine" : "Italian"}, 
#        {"address.zipcode" : "10075"}, 
#        {"grades.score" : {"$gt" : 20}}
#        ]})
#    cursor = rCol.find({"grades.score" : {"$gt" : 30}})
#    cursor.sort([
#        ("address.zipcode", pymongo.ASCENDING)
#    ])
#    for document in cursor:
#        print(document["address"])
#    result = rCol.update_one(
#        {"name" : "Juni"},
#        {
#            "$set" : {
#                "cuisine" : "America (new)"
#            },
#            "$currentDate" : {"lastModified" : True}
#        }
#    )
#    result = rCol.update_many(
#        {"address.zipcode" : "10016", "cuisine" : "Other"},
#        {
#            "$set" : {"cuisine" : "Category to be determined"},
#            "$currentDate" : {"lastModified" : True}
#        }
#    )
#    print("Match count: %s -- Modified count: %s" % (result.matched_count, result.modified_count))
    query = {"stream.unitOfMeasurement.name" : "degree Celsius"}
    cursor = rCol.find(query).limit(2)
    for doc in cursor:
        del doc["_id"]
        jdoc = json.dumps(doc, indent = 4)
        print("%s\n\n" % jdoc)
    


if __name__ == "__main__":
    testMongoDB()
