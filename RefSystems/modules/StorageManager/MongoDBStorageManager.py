# -*- coding: utf-8 -*-
"""
Created on Sat Apr  1 13:30:02 2017

@author: Nguyen Tran
"""
from Abstracts.Abstracts import AbsStorageManager
import pymongo
from pymongo import MongoClient

class MongoDBStorageManager(AbsStorageManager):
    def __init__(self, *args):
        super(MongoDBStorageManager, self).__init__(*args)
        self.addAttribute("host", self.connInfo, "host")
        self.addAttribute("port", self.connInfo, "port")
        self.addAttribute("db", self.connInfo, "db")
        self.addAttribute("col", self.connInfo, "col")
        self._connect()
    
    def insert(self, datum, insParams):
        print("%s receives following datum: %s\n\n" % ("MongoDBStorageManager", datum))
        pass

    def _connect(self):
        connAddr = "%s:%s" % (self.connInfo["host"], self.connInfo["port"])
        print(connAddr)
        try:
            self.client = MongoClient(connAddr)
        except:
            raise RuntimeError("Cannot connect to MongoDB")
    
    def create(self, datum):
        db, col = self.getDBCol(self.db, self.col)
        try:
            col.insert_one(datum["content"])
#            print("MongoDBStorageManager inserted %s to database\n\n" % datum["content"])
        except:
            print("Cannot insert datum %s into database" % datum)
            pass

    def read(self, queryObject, limit = 0):
        db, col = self.getDBCol(self.db, self.col)
        results = []
        try:
            for item in col.find(queryObject).limit(limit):
                del item["_id"]
                results.append(item)
            return results
        except:
            return None
    
    def update(self, params):
        pass
    
    def delete(self, params):
        pass
    
    def getDBCol(self, dbName, colName):
        db = self.client[dbName]
        col = db[colName]
        return db, col
            