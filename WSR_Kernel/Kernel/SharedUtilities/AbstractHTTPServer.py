# -*- coding: utf-8 -*-
"""
Created on Sun Apr 23 12:02:26 2017

@author: Nguyen Tran
This class represent an abstract threaded HTTP server as the interface to WSR
It serves as the base for concrete interface design provided by the user
"""
from Kernel.Abstracts.Abstracts import AbsInterface
from BaseHTTPServer import BaseHTTPRequestHandler,HTTPServer
from SocketServer import ThreadingMixIn

class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    """Handle HTTP requests in separated threads"""

class AbsHTTPInterface(AbsInterface):    
    def __init__(self, *args):
        super(AbsHTTPInterface, self).__init__(*args)
        self.addAttribute("host", self.moduleParams, "host")
        self.addAttribute("port", self.moduleParams, "port")
        # Setup handler object
        self.handler = None
        
    def startServer(self):
        try:
            server = ThreadedHTTPServer((self.host, int(self.port)), self.handler)
            print('Started httpserver on port %s' % self.port)
            server.serve_forever()
        
        except KeyboardInterrupt:
        	print '^C received, shutting down the web server'
        	server.socket.close()
            
    # This runnable module spawns a thread to start the HTTP server and stop the thread immediately
    def _proc(self):
        self.startServer()
        self.stop()
