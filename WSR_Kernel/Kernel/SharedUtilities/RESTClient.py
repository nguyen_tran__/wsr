# -*- coding: utf-8 -*-
"""
Created on Thu Mar 30 11:03:16 2017

@author: Nguyen Tran

This is a client for receiving and parsing JSON response from an external host.
It is a wrapper for uniRest library
"""
import httplib2 as http
import json
from Abstracts.Abstracts import AbsModule
        
try:
    from urlparse import urlparse
except ImportError:
    from urllib.parse import urlparse

class RESTClient(AbsModule):
    def __init__(self, *args):
        args = args + ("CLIENT",)
        super(RESTClient, self).__init__(*args)            
        self.h = http.Http()
    
    """
    Get a JSON response synchronously from a remote host
    """
    def getResponseJSON(self, uri, path = '', method = 'GET', headers = {'Accept' : 'application/json', 'Content-Type' : 'application/json; charset = UTF-8'},
                        body = ''):
        target = urlparse(uri + path)
        response, content = self.h.request(target.geturl(), method, body, headers)
        return json.loads(content)

if __name__ == "__main__":
    url = "http://localhost:8500/v1.0/"
    client = RESTClient()
    print(client.getResponseJSON("http://localhost:8500/v1.0/", "Sensors").get("@iot.count"))
        