# -*- coding: utf-8 -*-
"""
Created on Wed Apr 19 14:30:13 2017

@author: Nguyen Tran
"""
import os
import sys, getopt
import json
import re

import module_manager

class WSR (object):
    def __init__(self, argv):
        configJSONPath, modulesPath, tasks = self._getPaths(argv)
        # Add path to the root of the module for importing from abstracts
        currPath = os.path.dirname(os.path.realpath(__file__))
        sys.path.append(os.path.join(currPath, ".."))
        # Loading config JSON and setting up path to modules
        self.configJSONPath = configJSONPath
        self.modulesPath = modulesPath
        with open(self.configJSONPath, 'r') as inFile:
            self.configJSON = json.loads(inFile.read())
        sys.path.append(self.modulesPath)
        # Perform initialization
        if "d" in tasks:
            self.loadDiscoverer()
        if "i" in tasks:
            self.loadIndexer()
        if "s" in tasks:
            self.loadSearcher()
        
    """
    This utility function parses the given list of arguments and the paths to
    configuration JSON and modules
    """
    def _getPaths(self, argv):
        configJSONPath = ""
        modulesPath = ""
        tasks = ""
        try:
            opts, args = getopt.getopt(argv, "hc:m:dis", ["conf=","modules="])
        except getopt.GetoptError:
            print "args: -c <configJSON file> -m <path to modules>"
            sys.exit(2)
        
        if len(opts) == 0:
            print "args: -c <configJSON file> -m <path to modules>"
            sys.exit(2)
        
        for opt, arg in opts:
            if opt == "-h":
                print "args: -c <configJSON file> -m <path to modules>"
                sys.exit()
            elif opt in ("-c", "--conf"):
                if not(arg):
                    print "The given path to configuration JSON is invalid"
                    sys.exit(2)
                else:
                    configJSONPath = arg
            elif opt in ("-m", "--modules"):
                if not(arg):
                    print "The given path to modules is invalid"
                    sys.exit(2)
                else:
                    modulesPath = arg
            elif opt == "-d":
                tasks += "d"
            elif opt == "-i":
                tasks += "i"
            elif opt == "-s":
                tasks += "s"
            
        return configJSONPath, modulesPath, tasks
        
    """
    Give the type of module as specified in the configuration JSON script,
    this function find all module infos in the JSON having the name [type_name]
    """
    def _findRelevantModuleInfos(self, moduleType, onlyOne = False):
        pattern = r"%s_?(\S*)" % moduleType
        moduleInfos = {}
        for key in self.configJSON["modules"]:
            m = re.match(pattern, key)
            if m:
                moduleInfos[m.group(1)] = self.configJSON["modules"][key]
        if len(moduleInfos.keys()) == 0:
            raise ValueError("No %s module was given. Module must be declared in form %s_[name]" % (moduleType, moduleType))
        if onlyOne and not len(moduleInfos.keys()) == 1:
            raise ValueError("Only one instance of %s is allowed." % moduleType)
        return moduleInfos
        
    """
    Load detectors, collectors and their preprocessing chains
    """
    def loadDiscoverer(self):
        # Create a shared memory object for involving discoverer and collectors
        from Abstracts.detected_resources import DetectedResources
        detectedRes = DetectedResources()
        self._loadDetectors(detectedRes)
        self._loadCollectors(detectedRes)
        
    """
    Load indexer component
    """
    def loadIndexer(self):
        moduleInfos = self._findRelevantModuleInfos("indexer")
        for key in moduleInfos:
            module_manager.loadModules(moduleInfos[key], systemParams = [], moduleTypes = ["INDEXER"])
        return moduleInfos.keys();
    
    def loadSearcher(self):
        (queryQueues, resultQueues), _ = self._loadScorer()
        self._loadUserInterface(queryQueues, resultQueues)
    
    """
    Load scorer component
    """
    def _loadScorer(self):
        import Queue
        moduleInfos = self._findRelevantModuleInfos("scorer", onlyOne = True)
        queryQueues = []
        resultQueues = []
        for key in moduleInfos:
            for i in range(moduleInfos[key][0]["duplicates"]):
                queryQueue = Queue.Queue()
                resultQueue = Queue.Queue()
                module_manager.loadModules(moduleInfos[key], systemParams = [queryQueue, resultQueue], moduleTypes = ["SEQ_SCORER_HEAD", "PARA_SCORER_HEAD"])
                queryQueues.append(queryQueue)
                resultQueues.append(resultQueue)
        return (queryQueues, resultQueues), moduleInfos.keys();
        
    """
    Load user interface component
    """
    def _loadUserInterface(self, queryQueues, resultQueues):
        moduleInfos = self._findRelevantModuleInfos("interface", onlyOne = True)
        for key in moduleInfos:
            module_manager.loadModules(moduleInfos[key], systemParams = [queryQueues, resultQueues], moduleTypes = ["INTERFACE"])
        return moduleInfos.keys();
    
    """
    Load collectors and its related preprocessing chain
    """
    def _loadCollectors(self, detectedRes):
        # Load collectors and its preprocessing chain
        moduleInfos = self._findRelevantModuleInfos("collector")
        import Queue
        for key in moduleInfos:
            queue = Queue.Queue()
            module_manager.loadModules(moduleInfos[key], systemParams = [detectedRes, queue], moduleTypes = ["COLLECTOR"])
            self._loadPreprocessor(key, queue)
        return moduleInfos.keys()
    
    def _loadPreprocessor(self, collectorName, queue):
        moduleInfos = self._findRelevantModuleInfos("preprocessor_%s" % collectorName, onlyOne = True)
        for key in moduleInfos:
            module_manager.loadModules(moduleInfos[key], systemParams = [queue], moduleTypes = ["PROC_HEAD"])
        return moduleInfos.keys()
        
    """
    Load detectors
    """
    def _loadDetectors(self, detectedRes):
        moduleInfos = self._findRelevantModuleInfos("detector")
        for key in moduleInfos:
            module_manager.loadModules(moduleInfos[key], systemParams = [detectedRes], moduleTypes = ["DETECTOR"])
        return moduleInfos.keys()
    
        
        
            
if __name__ == "__main__":
    pass
    wsr = WSR("C:\Users\gentr\Documents\CodeProjects\WSR\configs\init.json", "")
    wsr.loadDiscoverer()
    wsr.loadIndexer()
    wsr.loadSearcher()