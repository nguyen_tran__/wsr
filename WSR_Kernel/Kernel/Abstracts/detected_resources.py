# -*- coding: utf-8 -*-
"""
Created on Wed Mar 29 11:18:28 2017

@author: Nguyen Tran

This is a data model object for sharing results between detectors and collectors
"""


class DetectedResources(object):
    def __init__(self):
        self.type = "DETECTED_RES"
        self.sourceURLs = []
        self.sensorURLs = []
        self.datastreamURLs = []
        self.resources = {"SOURCE" : self.sourceURLs,
                          "SENSOR" : self.sensorURLs,
                          "DATASTREAM" : self.datastreamURLs}

    def addSourceURL(self, sourceURL):
        self._addURL("sourceURLs", sourceURL)

    def addSensorURL(self, sensorURL):
        self._addURL("sensorURLs", sensorURL)

    def addDatastreamURL(self, datastreamURL):
        self._addURL("datastreamURLs", datastreamURL)

    def _addURL(self, urlType, url):
        urls = getattr(self, urlType)
        urls.append(url)
        
    def addRes(self, resType, datum):
        try:
            resList = self.resources.get(resType)
            resList.append(datum)
        except:
            raise RuntimeError("Cannot add the datum %s of type %s into detected resources object" % (datum, resType))
        
    def getSources(self):
        return self._getURLs("sourceURLs")
        
    def getSensors(self):
        return self._getURLs("sensorURLs")
        
    def getDatastreams(self):
        return self._getURLs("datastreamURLs")
        
    def _getURLs(self, sourceType):
        return getattr(self, sourceType)

        
if __name__ == "__main__":
    dr = DetectedResources()
    for i in range(5):
        dr.addDatastreamURL("http://localhost:8%s00/v1.0/Sensors(%s)/Datastreams(%s)" % (i, i, i))
    print(dr.getDatastreams())
