# -*- coding: utf-8 -*-
"""
Created on Sun Apr 23 12:14:02 2017

@author: Nguyen Tran
This class represents the response sent from scorer head to the user interface
"""
class Response (object):
    def __init__(self, clientInfo, result):
        if not(getattr(result, "type", "") == "RESULT"):
            raise RuntimeError("The result object given to this response is not a Result object")
        else:
            self.clientInfo = clientInfo
            self.result = result
            self.type = "RESPONSE"
