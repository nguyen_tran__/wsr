# -*- coding: utf-8 -*-
"""
Created on Wed Apr 19 18:43:39 2017

@author: Nguyen Tran
Definition of all abstract modules of WSR
"""
import threading
import Queue
import time
from Result import Result
from Query import Query
from Request import Request
from Response import Response

"""
=======================================================
Core Abstract Module - Ancestor of all modules
=======================================================
"""
class AbsModule(object):
    def __init__(self, *args):
        self.systemParams = args[0]
        self.moduleParams = args[1]
        self.subModules = args[2]
        self.type = args[3]
        
    def addAttribute(self, attrName, fromParams, index, moduleType = None):
        errMes = "Cannot find %s in %s. It must be supplied at index %s" % (attrName, fromParams, index)
        try:
            attr = fromParams[index]
            if (moduleType is not None) and (moduleType != getattr(attr, "type")):
                print(attr, "type")
                raise ReferenceError(errMes)
        except:
            raise ReferenceError(errMes)
        setattr(self, attrName, attr)
        
"""
=======================================================
Abstract Runnable Module
=======================================================
"""
class AbsRunnableModule(AbsModule):
    def __init__(self, *args):
        super(AbsRunnableModule, self).__init__(*args)
        self.t = threading.Thread(target=self._loop)
        self.isRunning = False
        
    def start(self):
        self.isRunning = True
        self.t.start()
    
    def stop(self):
        self.isRunning = False
    
    def _loop(self):
        while self.isRunning:
            self._proc()
    
    def _proc(self):
        raise NotImplementedError("Please implement the _proc method of this runnable module")


"""
=======================================================
Abstract Processing Head
=======================================================
"""
class AbsProcessingHead(AbsRunnableModule):
    def __init__(self, *args):
        super(AbsProcessingHead, self).__init__(*args)
        
    """
    Processing head is the last chain in a processing chain
    These functions mirrors the process functions of processing chains to allow
    implementing the last processing chain
    """
    def process(self, datum, result):
        if datum is not None:
            self._processDatum(datum, result)
        else:
            pass
    
    def _processDatum(self, datum, result):
        raise NotImplementedError("Please implement the _processDatum method of preprocessor head")
    
    """
    This method invoke the whole processing chain. How this invocation is done
    is specified by child classes
    """
    def _startProcessing(self, datum):
        raise NotImplementedError("Please implement the _startProcessing method of preprocessor head")

    """
    This method transform the datum and generate an empty result object to be used
    by the processing chain
    """
    def _initializeDatumResult(self, datum):
        raise NotImplementedError("Please implement the _initializeDatumResult method of preprocessor head")

"""
=======================================================
Abstract Processing Chain
=======================================================
"""
class AbsProcessingChain(AbsModule):
    def __init__(self, *args):
        args = args + ("PROC_CHAIN",)
        super(AbsProcessingChain, self).__init__(*args)
        self.next = None
        self.queue = None
    
    def setNextChain(self, nextChain):
        try:
            procFunct = getattr(nextChain, "process", None)
            if procFunct is None:
                raise RuntimeError("The specified next processing chain at %s is invalid" % self)
            self.next = nextChain
        except:
            raise RuntimeError("The specified next processing chain at %s is invalid" % self)
            
    def setQueue(self, queue):
        self.queue = queue
    
    def process(self, datum, result):
        if datum is not None:
            self._processDatum(datum, result)
            # If the queue is already set, processing chain will put the result to the queue
            if self.queue is not None:
                self.queue.put(result)
            # Call the next chain if it is available
            if self.next is not None:
                self.next.process(datum, result)
    
    def _processDatum(self, datum, result):
        raise NotImplementedError("Please implement the _processDatum method of preprocessor head")

"""
=======================================================
Abstract Sequential Processing Head
=======================================================
"""
class AbsSeqProcHead (AbsProcessingHead):
    def __init__(self, *args):
        super(AbsSeqProcHead, self).__init__(*args)
        self.head = None
        
    """
    Linking chains specified in sub modules with each other and with the processing head.
    Offset = number of modules at the end of the module list that are not processing chain
    """
    def _setupChain(self, offset):
        for i in range(len(self.subModules) - offset):
            # Setup the first module as the head of the chain
            if i == 0:
                self.head = self.subModules[i]
            # if processing the last chain, set its next chain to this class
            if (i+1) == len(self.subModules) - offset:
                self.subModules[i].setNextChain(self)
            else:
                self.subModules[i].setNextChain(self.subModules[i+1])
                
    def _startProcessing(self, datum):
        datum, result = self._initializeDatumResult(datum)
        if(result.isError):
            print("Given datum is invalid")
            return result
        else:
            return self.head.process(datum, result)

"""
=======================================================
Abstract Parallel Processing Head
=======================================================
"""
class AbsParaProcHead (AbsProcessingHead):
    def __init__(self, *args):
        super(AbsParaProcHead, self).__init__(*args)
        self.procChains = self.subModules
        self.chainQueue = Queue.Queue()
        self.addAttribute("timeout", self.moduleParams, "timeout")
        self._setChainQueue()
        
    def _startProcessing(self, datum):
        datum, result = self._initializeDatumResult(datum)
        if result.isError:
            print("Datum is invalid (the result object set to error object)")
            return result
        # Start a new thread for every chain in the list
        for chain in self.procChains:
            t = threading.Thread(target = chain.process, args = [datum, result])
            t.start()
        # Retrieve all processing results from the chain.
        results = []
        timeouts = 0
        while (len(results) + timeouts) < len(self.procChains):
            try:
                res = self.chainQueue.get(timeout = self.timeout)
                results.append(res)
            except Queue.Empty:
                timeouts = timeouts + 1
                print("Timeouts: %s" % timeouts)
        aggregatedResult = self._aggregateResults(datum, results)
        return aggregatedResult
            
            
    def _validateProcChains(self):
        raise NotImplementedError("Please implement the _validateProcChain method of this Parallel processing head")
            
    def _setChainQueue(self):
        for chain in self.procChains:
            try:
                chain.setQueue(self.chainQueue)
            except:
                raise RuntimeError("Failed to assign queue to chain %s. This chain object might be invalid" % chain)
    
    def _aggregateResults(self, datum, results):
        raise NotImplementedError("Please implement the _aggregateResults method of this Parallel processing head")

"""
=======================================================
Abstract Discoverer Module
=======================================================
"""
class AbsDiscovererModule(AbsRunnableModule):
    def __init__(self, *args):
        super(AbsDiscovererModule, self).__init__(*args)
        # Setup detected resource attribute
        self.addAttribute("detectedRes", self.systemParams, 0, moduleType = "DETECTED_RES")
        self.processedRes = []
        # Setup res type attribute     
        self.addAttribute("resType", self.moduleParams, "resType")
        # Setup connection client attribute
        self.addAttribute("connClient", self.subModules, 0, moduleType = "CLIENT")
        
    def _proc(self):
        self._detect()
    
    def _detect(self):
        raise NotImplementedError("Please implement the _detect method of this discoverer module")
        
"""
=======================================================
Abstract Detector Module
=======================================================
"""
class AbsDetectorModule(AbsDiscovererModule):
    def __init__(self, *args):
        args = args + ("DETECTOR",)
        super(AbsDetectorModule, self).__init__(*args)
        
    def _detectRes(self):
        raise NotImplementedError("Please implement the _detectRes method of this detector module")
        
    def _addRes(self, datum):
        try:
            self.detectedRes.addRes(self.resType, datum)
#            print("Following resource is detected: %s -- %s\n" % (self.resType, datum))
        except:
            raise RuntimeError("Cannot add datum into detected resource object")
            
    def _detect(self):
        self._detectRes()

"""
=======================================================
Abstract Collector Module
=======================================================
"""
class AbsCollectorModule(AbsDiscovererModule):
    def __init__(self, *args):
        args = args + ("COLLECTOR",)
        super(AbsCollectorModule, self).__init__(*args)
        self.addAttribute("queue", self.systemParams, 1)
    
    def _collect(self):
        raise NotImplementedError("Please implement the _collect method of this collector module")
        
    def _getRes(self, resType):
        try:
            return self.detectedRes.getRes(resType)
        except:
            raise RuntimeError("Cannot get resources of type %s from detected resources object" % resType)
            
    def _addToQueue(self, datum):
        self.queue.put(datum)
#        print("Collector put following datum to queue: %s" % datum)
        
    def _detect(self):
        self._collect()

"""
=======================================================
Abstract Indexer
=======================================================
"""
class AbsIndexer (AbsRunnableModule):
    def __init__(self, *args):
        args = args + ("INDEXER", )
        super(AbsIndexer, self).__init__(*args)
        self.addAttribute("storageManager", self.subModules, 0)
        self.isCreated = False
        
    def _buildIndex():
        raise NotImplementedError("Please implement the _buildIndex method of this indexer module")
        
    def _reIndex():
        raise NotImplementedError("Please implement the _reIndex method of this indexer module")

"""
=======================================================
Abstract One Time Indexer
=======================================================
"""
class AbsOneTimeIndexer (AbsIndexer):
    def __init__(self, *args):
        super(AbsOneTimeIndexer, self).__init__(*args)
        
    def _proc(self):
        if not self.isCreated:
            self._buildIndex()
            self.isCreated = True
        self.stop()

"""
=======================================================
Abstract Period Indexer
=======================================================
"""
class AbsPeriodIndexer (AbsIndexer):
    def __init__(self, *args):
        super(AbsPeriodIndexer, self).__init__(*args)
        self.addAttribute("refreshRate", self.moduleParams, "refreshRate")
        
    def _proc(self):
        if not self.isCreated:
            self._buildIndex()
            self.isCreated = True
        else:
            self._reIndex()
        time.sleep(self.refreshRate)
 
"""
=======================================================
Abstract Reactive Indexer
=======================================================
"""
class AbsReactiveIndexer (AbsIndexer):
    def __init__(self, *args):
        super(AbsReactiveIndexer, self).__init__(*args)
        self.addAttribute("queue", self.systemParams, 0)
        
    def _proc(self):
        if not self.isCreated:
            self._buildIndex()
        else:
            # Block until a new resource is retrieved
            self.queue.get()
            self._reIndex()
       
"""
=======================================================
Abstract Preprocessing Head
=======================================================
"""      
class AbsPreProcHead (AbsSeqProcHead):
    def __init__(self, *args):
        args = args + ("PROC_HEAD",)
        super(AbsPreProcHead, self).__init__(*args)
        self.addAttribute("queue", self.systemParams, 0)
        self.addAttribute("storageManager", self.subModules, -1, moduleType="STORAGE")
        self._setupChain(1)
        
    def _initializeDatumResult(self, datum):
        result = Result()
        return datum, result
    
    def _proc(self):
        datum = self.queue.get()
        self._startProcessing(datum)
#        print("Preprocessing finished for: %s" % datum)
        self.queue.task_done()
        
"""
=======================================================
Abstract Preprocessing Chain
=======================================================
"""  
class AbsPreProcChain (AbsProcessingChain):
    def __init__(self, *args):
        args = args + ("PROC_CHAIN",)
        super(AbsPreProcChain, self).__init__(*args)

"""
=======================================================
Abstract Scorer
=======================================================
"""  
class AbsScorer (AbsProcessingChain):
    def __init__(self, *args):
        super(AbsScorer, self).__init__(*args)
        
    def _processDatum(self, datum, result):
        if datum.type is not "QUERY" and result.type is not "RESULT":
            raise TypeError("Given datum and result for scorer chain %s are invalid" % self)
        else:
            self._score(datum, result)
    
    def _score(self, query, result):
        raise NotImplementedError("Please implement the _score method of this scorer chain")

"""
=======================================================
Abstract Sequential Scorer Head
=======================================================
""" 
class AbsSeqScorerHead(AbsSeqProcHead):
    def __init__(self, *args):
        args = args + ("SEQ_SCORER_HEAD",)
        super(AbsSeqScorerHead, self).__init__(*args)
        self.addAttribute("queue", self.systemParams, 0)
        self.addAttribute("resultQueue", self.systemParams, 1)
        self._setupChain(0)
        self._validateProcHead()
        
    def _initializeDatumResult(self, datum):
        try:
            if getattr(datum, "type", "") == "QUERY":
                query = datum
            else:
                query = Query(*datum)
            result = Result()
            return query, result
        except:
            print("The given datum is not a valid query object")
            result = Result(err = True, errorMes = "The given datum is not a valid query object")
            return datum, result
    
    def _proc(self):
        request = self.queue.get()
        datum = request.query
        clientInfo = request.clientInfo
        aggregatedResult = self._startProcessing(datum)
        self.resultQueue.put(Response(clientInfo, aggregatedResult))
        self.queue.task_done()
        
    def _validateProcHead(self):
        try:
            if self.head.type != "SCORER_RETRIEVER":
                raise TypeError("The first item of a scorer chain must be a retriever scorer")
        except:
            raise TypeError("The current head of the scorer chain is not a scorer")

    def _processDatum(self, datum, result):
        if datum.type is "QUERY" and result.type is "RESULT":
            self._processResults(datum, result)
        else:
            raise RuntimeError("The datum and result received at the scorer head are invalid")
            
    def _processResults(query, result):
        raise NotImplementedError("Please implement the _processResults method of this scorer head")

"""
=======================================================
Abstract Parallel Scorer Head
=======================================================
""" 
class AbsParaScorerHead(AbsParaProcHead):
    def __init__(self, *args):
        args = args + ("PARA_SCORER_HEAD",)
        super(AbsParaScorerHead, self).__init__(*args)
        self.addAttribute("queue", self.systemParams, 0)
        self.addAttribute("resultQueue", self.systemParams, 1)
        self._validateProcChains()
        
    def _initializeDatumResult(self, datum):
        try:
            if getattr(datum, "type", "") == "QUERY":
                query = datum
            else:
                query = Query(*datum)
            result = Result()
            return query, result
        except:
            print("The given datum is not a valid query object")
            result = Result(error = True, errorMes = "The given datum is not a valid query object")
            return datum, result
            
    def __proc(self):
        request = self.queue.get()
        t = threading.Thread(target = self.__processQuery, args=[request])
        t.start()
        
    def _proc(self):
        request = self.queue.get()
        datum = request.query
        clientInfo = request.clientInfo
        aggregatedResult = self._startProcessing(datum)
        self.resultQueue.put(Response(clientInfo, aggregatedResult))
        self.queue.task_done()
        
    def _validateProcChains(self):
        for chain in self.procChains:
            try:
                print(chain)
                if chain.type != "SCORER_RETRIEVER":
                    raise TypeError("Scorers of a parallel scorer head must be retriever")
            except:
                raise TypeError("A chain of this parallel scorer head is not a scorer")
                
    def __processQuery(self, request):
        datum = request.query
        clientInfo = request.clientInfo
        aggregatedResult = self._startProcessing(datum)
        self.resultQueue.put(Response(clientInfo, aggregatedResult))
        self.queue.task_done()
#        print(threading.current_thread())
                
"""
=======================================================
Abstract Retriever Scorer
=======================================================
""" 
class AbsRetrieverScorer(AbsScorer):
    def __init__(self, *args):
        args = args + ("SCORER_RETRIEVER",)
        super(AbsRetrieverScorer, self).__init__(*args)
        self.addAttribute("storageManager", self.subModules, 0)
        
    def _score(self, query, result):
        self._retrieve(query, result)
        
    def _retrieve(query, result):
        raise NotImplementedError("Please implement the _retrieve method of this retriever scorer chain")
        
"""
=======================================================
Abstract Filter Scorer
=======================================================
""" 
class AbsFilterScorer(AbsScorer):
    def __init__(self, *args):
        args = args + ("SCORER_FILTER",)
        super(AbsFilterScorer, self).__init__(*args)
        
    def _score(self, query, result):
        self._filter(query, result)
        
    def _filter(self, query, result):
        raise NotImplementedError("Please implement the _filter method of this filter scorer chain")

"""
=======================================================
Abstract User Interface Interface
=======================================================
""" 
class AbsInterface(AbsRunnableModule):    
    def __init__(self, *args):
        args = args + ("INTERFACE",)
        super(AbsInterface, self).__init__(*args)
        self.addAttribute("queryQueues", self.systemParams, 0)
        self.addAttribute("resultQueues", self.systemParams, 1)
        # Add query and result queues into a pool to share between clients
        self.scorerConnQueue = Queue.Queue()
        for i in range(len(self.queryQueues)):
            self.scorerConnQueue.put((self.queryQueues[i], self.resultQueues[i]))
        
    def _getScorerConn(self):
        (queryQueue, resultQueue) = self.scorerConnQueue.get()
        return queryQueue, resultQueue
        
    def _putRequest(self,clientInfo, queryInfo, queryQueue):
        query = self._createQueryObject(queryInfo)
        req = Request(clientInfo, query)
        queryQueue.put(req)
        
    def _getResponse(self, pendingClientInfo, resultQueue):
        response = resultQueue.get()
        # If the response belongs to the pending client, get it, otherwise put it back to queue
        if self._compClientInfo(pendingClientInfo, response.clientInfo):
            return response
        else:
            resultQueue.put(response)
            return None
    def _returnScorerConn(self, queryQueue, resultQueue):
        self.scorerConnQueue.put((queryQueue, resultQueue))
    
    def _compClientInfo(self, pendingClientInfo, clientInfo):
        raise NotImplementedError("Please implement the _compClientInfo method of this interface module")
        
    def _createQueryObject(self, queryInfo):
        raise NotImplementedError("Please implement the _createQueryObject method of this interface module")

"""
=======================================================
Abstract Storage Manager
=======================================================
""" 
class AbsStorageManager(AbsModule):
    def __init__(self, *args):
        args = args + ("STORAGE",)
        super(AbsStorageManager, self).__init__(*args)
        self.addAttribute("connInfo", self.moduleParams, "connInfo")
        
    def _connect(self):
        raise NotImplementedError("Please implement the _connect method of this storage module")
        
    def create(self, *args, **kwargs):
        raise NotImplementedError("Please implement the create method of this storage module")
        
    def read(self, *args, **kwargs):
        raise NotImplementedError("Please implement the read method of this storage module")
        
    def update(self, *args, **kwargs):
        raise NotImplementedError("Please implement the update method of this storage module")
        
    def delete(self, *args, **kwargs):
        raise NotImplementedError("Please implement the delete method of this storage module")
