# -*- coding: utf-8 -*-
"""
Created on Wed Apr 05 11:31:13 2017

@author: Nguyen Tran
"""
class Query (object):
    def __init__(self, *args):
        if len(args) != 5:
            raise RuntimeError("A query must contain [5] sets of query criteria")
        
        attrNames = ["SensorMetadata", "FoI", "Location", "Observation", "QoS"]
        for i in range(5):
            if not self.__addAttribute(attrNames[i], args[i]):
                raise RuntimeError("Each query criterion must be specified as a dictionary")
        self.type = "QUERY"
    
    def __addAttribute(self, attrName, attr):
        if type(attr) is dict:
            decodedAttr= {}
            # Decode attr to string
            for key in attr:
                decodedAttr[str(key)] = str(attr[key])
            setattr(self, attrName, decodedAttr)
            return True
        else:
            return False
        
    def __str__(self):
        rep = ""
        attrNames = ["SensorMetadata", "FoI", "Location", "Observation", "QoS"]
        for i in range(5):
            rep += "%s : %s\n" % (attrNames[i], getattr(self, attrNames[i]))
        return rep
